package ru.tsc.apozdnov.tm;

import ru.tsc.apozdnov.tm.constant.TerminalConstant;

public final class Application {

    public static void main(final String[] args) {
        processTask(args);
    }

    public static void processTask(final String[] args) {
        if (args == null || args.length == 0) return;
        final String arg = args[0];
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case TerminalConstant.ABOUT:
                showAbout();
                break;
            case TerminalConstant.VERSION:
                showVersion();
                break;
            case TerminalConstant.HELP:
                showHelp();
                break;
            default:
                showFault(arg);
                break;
        }
    }

    public static void showFault(String fault) {
        System.err.printf("Fault... This argument `%s` not supported...  \n", fault);
    }

    public static void showAbout() {
        System.out.println("[About]");
        System.out.println("[Name: Aleksandr Pozdnov]");
        System.out.println("[E-mail: apozdnov@t1.com]");
    }

    public static void showVersion() {
        System.out.println("1.2.0");
    }

    public static void showHelp() {
        System.out.println("[HELP]");
        System.out.format("%s - Show developer info.\n", TerminalConstant.ABOUT);
        System.out.format("%s - Show application version.\n", TerminalConstant.VERSION);
        System.out.format("%s - Show terminal commands.\n", TerminalConstant.HELP);
    }

}
